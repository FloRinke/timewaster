# TimeWaster
Organize your time-wastage moar efficiently
(at least as far as this concerns watching series, everything else you still need to do manually)

- Track Shows you want to, are currently or did already watch
- Check for new episodes, and which you have watched already
- Generate and manage watchlists for unseen episodes
