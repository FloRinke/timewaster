from django.contrib import admin

from . import models


class EpisodeInlineAdmin(admin.TabularInline):
    model = models.Episode
    readonly_fields = [
        "created",
        "last_updated",
    ]
    fields = [
        "pubdate",
        "num",
        "title",
        ]
    extra = 0


@admin.register(models.Show)
class ShowAdmin(admin.ModelAdmin):
    list_display = [
        "mal_id",
        "name_en",
        "name_jp_romaji",
        "name_jp",
        "comment",
        "release_date",
        "source_type",
        "source",
    ]
    readonly_fields = [
        "created",
        "last_updated",
    ]
    inlines = [EpisodeInlineAdmin, ]


@admin.register(models.Episode)
class EpisodeAdmin(admin.ModelAdmin):
    list_display = [
        "num",
        "title",
        "pubdate",
    ]
    readonly_fields = [
        "created",
        "last_updated",
    ]
