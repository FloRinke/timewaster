from rest_framework import viewsets, permissions

from . import serializers
from . import models


class ShowViewSet(viewsets.ModelViewSet):
    """ViewSet for the show class"""

    queryset = models.Show.objects.all()
    serializer_class = serializers.ShowSerializer
    permission_classes = [permissions.IsAuthenticated]


class EpisodeViewSet(viewsets.ModelViewSet):
    """ViewSet for the episode class"""

    queryset = models.Episode.objects.all()
    serializer_class = serializers.EpisodeSerializer
    permission_classes = [permissions.IsAuthenticated]
