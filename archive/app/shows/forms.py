from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.core import exceptions as django_exceptions

from .helper import mal
from . import models

BUTTON_SUBMIT_NAME = 'submit'
BUTTON_QUERY_NAME = 'query'

MAL_IMPORT_FIELDS = ['id', 'title', 'alternative_titles', 'start_date', 'end_date', 'media_type', 'status',
                     'num_episodes', 'start_season']
SEASON_MONTH_LOOKUP = {
    'winter': 1,
    'spring': 4,
    'summer': 7,
    'fall': 10,
}


class ShowForm(forms.ModelForm):
    class Meta:
        model = models.Show
        fields = [
            "name_en",
            "name_jp_romaji",
            "name_jp",
            "comment",
            "release_date",
            "source_type",
            "source",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit('submit', 'Save'))


class ShowImportForm(forms.ModelForm):
    # additional fields in form, not to be saved
    ack = forms.BooleanField(required=False)
    raw = forms.CharField(required=False, widget=forms.Textarea)

    class Meta:
        model = models.Show
        fields = [
            "mal_id",
            "name_en",
            "name_jp_romaji",
            "name_jp",
            "comment",
            "release_date",
            "source_type",
            "source",
            "ack",
        ]

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit(BUTTON_SUBMIT_NAME, 'Save'))
        self.helper.add_input(Submit(BUTTON_QUERY_NAME, 'Query MAL'))

        # If the form has been submitted, populate the disabled field
        if 'data' in kwargs and BUTTON_QUERY_NAME in kwargs['data']:
            data = kwargs['data'].copy()
            self.prefix = kwargs.get('prefix')
            mal_id = data['mal_id']
            anime = mal.get_anime(mal_id, MAL_IMPORT_FIELDS)
            data[self.add_prefix('raw')] = str(anime)

            if 'alternative_titles' not in anime:
                # if there are no specific names, use title as english name
                data[self.add_prefix('name_en')] = anime['title']
            else:
                if 'en' in anime['alternative_titles']:
                    # if there's a specific english name, store title in comment
                    data[self.add_prefix('name_en')] = anime['alternative_titles']['en']
                    data[self.add_prefix('name_jp_romaji')] = anime['title']
                else:
                    # otherwise assume main name is english for now
                    data[self.add_prefix('name_en')] = anime['title']
                if 'ja' in anime['alternative_titles']:
                    # store japanese name if available
                    data[self.add_prefix('name_jp')] = anime['alternative_titles']['ja']
                    if 'en' not in anime['alternative_titles']:
                        # if there is no specific english name, assume title is english
                        data[self.add_prefix('name_en')] = anime['title']
            if 'start_date' in anime:
                data[self.add_prefix('release_date')] = anime['start_date']
            elif 'start_season' in anime:
                month = SEASON_MONTH_LOOKUP[anime['start_season']['season']]
                year = anime['start_season']['year']
                data[self.add_prefix('release_date')] = f"{year}-{month}-01"
            kwargs['data'] = data
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        if 'submit' in self.data:
            if not cleaned_data['ack']:
                self.add_error('ack', "Check 'acknowledge' to save")
        elif 'query' in self.data:
            if 'mal_id' not in cleaned_data or cleaned_data['mal_id'] is None or cleaned_data['mal_id'] == '':
                self.add_error('mal_id', "Enter a MAL ID for lookup")
            raise django_exceptions.ValidationError("Running import, not saving yet. Please check/correct and save!")


class EpisodeForm(forms.ModelForm):
    class Meta:
        model = models.Episode
        fields = [
            "num",
            "title",
            "pubdate",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit('submit', 'Save'))
