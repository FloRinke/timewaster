import requests

MAL_API_ID = 'dummy'


def get_anime(mal_id: int, fields: list[str] | tuple[str]) -> dict:
    if isinstance(fields, str):
        payload = {'fields': fields}
    elif isinstance(fields, list) or isinstance(fields, tuple):
        payload = {'fields': ','.join(fields)}
    else:
        payload = {}
    req = requests.get(f"https://api.myanimelist.net/v2/anime/{mal_id}",
                       headers={'X-MAL-CLIENT-ID': MAL_API_ID},
                       params=payload)

    return req.json()
