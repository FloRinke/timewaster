import feedparser
import re


RE_EPISODECOUNT = [
    re.compile(r'S(?P<season>\d{2})E(?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?'),
    re.compile(r'(?P<season>\d+)x(?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?'),
    re.compile(r' - (?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?\b'),
    re.compile(r'\b(?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?\b'),
]

RE_ISBATCH = re.compile(r'Season|Batch')

BASEURL = "https://nyaa.si/?page=rss&c=1_2&f=0"


def parse_episodecount(haystack: str):
    for exp in RE_EPISODECOUNT:
        m = exp.search(haystack)
        if m is not None:
            # print("        matched: {}".format(m.groupdict()))
            return m.groupdict()
    return dict()


def get_feed(query: str):
    url = BASEURL + "&" + query
    print(f"fetching feed from URL '{url}'")
    f = feedparser.parse(url)
    episodes = []
    for ep in f.entries:
        episode_data = {
            'title': ep.title,
            'size': ep.nyaa_size,
            'dl': ep.link,
        }
        epcount = parse_episodecount(ep.title).items()
        # print("  recognized metadata: {}".format(epcount))
        episode_data.update({k: int(v) for k, v in epcount if v is not None})
        episodes.append(episode_data)
    return episodes
    # try:
    #     print("Found Source {}".format(f.feed.title.split('"')[1]), file=sys.stderr)
    #     return f
    # except Exception as ex:
    #     print("Error '' while polling source: {}".format(ex, f))


def parse_feed(fp, slug: str):
    result = []
    # title = fp.feed.subtitle.split('"')[1]
    for ep in fp.entries:
        is_batch = RE_ISBATCH.search(ep.title) is not None
        if not is_batch:
            # print("  - Found Episode: {}".format(ep.title))
            # print("      raw: {}".format(ep))
            # print("      size: {}".format(ep.nyaa_size))
            # print("      id: {}".format(ep.id))
            # print("      url: {}".format(ep.link))
            episode_data = {
                'slug': slug,
                # 'title': title,
                'size': ep.nyaa_size,
                # 'id': e.id,
                'dl': ep.link,
                # 'raw': e
            }
            epcount = parse_episodecount(ep.title).items()
            # print("      recognized metadata: {}".format(epcount))
            episode_data.update({k: int(v) for k, v in epcount if v is not None})
            # episode_data.update({k: int(v) for k, v in parse_episodecount(ep.title).items()})
            result.append(episode_data)
    return result