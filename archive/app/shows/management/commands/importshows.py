import argparse
import json
import random
from urllib.parse import urlsplit, parse_qs, urlencode

from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError

from shows import models


class Command(BaseCommand):
    help = 'Import show data from json file'

    @staticmethod
    def _parse_nyaa(url: str):
        # print(urlsplit(url).query)
        p = parse_qs(urlsplit(url).query)
        ret = {k: v[0] for k, v in p.items() if k == 'q' or k == 'u'}
        # print(ret)
        # print(urlencode(ret))
        return models.SOURCES.NYAA, urlencode(ret)

    def add_arguments(self, parser):
        parser.add_argument('file', type=argparse.FileType('r'), help="twcli local json file to import")
        parser.add_argument('tag', help="Select this tag on first level.")
        parser.add_argument('--date', '-d', default=None, help="Use as release date if not set.")
        parser.add_argument('--force-add', '-f', action='store_true', help="Force add existing (adds 4 random chars).")

    def handle(self, *args, **kwargs):
        file = kwargs['file']
        tag = kwargs['tag']
        if 'date' in kwargs:
            date = kwargs['date']
        else:
            date = None
        force_add = kwargs['force_add']

        data = json.loads(file.read())
        if tag not in data:
            raise CommandError(f"Tag {tag} not found in input file")

        for k in data[tag]:
            show = data[tag][k]
            s = models.Show()

            # use index as slug
            # s.slug = k

            # import titles
            if "title_en" in show:
                s.name_en = show["title_en"]
                del show["title_en"]
            if "title_jp" in show:
                s.name_jp = show["title_jp"]
                del show["title_jp"]

            # parse nyaa source url
            if "rss" in show:
                if show["rss"][0:16] == 'https://nyaa.si/':
                    s.source_type, s.source = self._parse_nyaa(show["rss"])
                else:
                    s.source = show["rss"]
                del show["rss"]

            if date is not None:
                s.release_date = date

            # attach anything else as comment
            if len(show) > 0:
                s.comment = str(show)

            try:
                s.save()
                self.stdout.write(f"New show: {s}")
            except IntegrityError:
                # slug already in use
                if force_add:
                    # append random chars so UNIQUE is satisfied:
                    s.slug += "_" + "".join(random.choices('abcdefghijklmnopqrstuvwxyz0123456789', k=4))
                    s.save()
                    self.stdout.write(f"Force-added new show {k} as {s.slug}")
                else:
                    self.stderr.write(f"Ignoring show {s.slug}, already in db. Use --force to add anyway")
