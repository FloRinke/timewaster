# Generated by Django 3.2.13 on 2022-05-15 17:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Show',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('slug', models.SlugField(unique=True)),
                ('name_jp', models.CharField(blank=True, max_length=255, null=True)),
                ('name_en', models.CharField(blank=True, max_length=255, null=True)),
                ('comment', models.CharField(blank=True, max_length=255, null=True)),
                ('release_date', models.DateField(blank=True, null=True)),
                ('source_type', models.CharField(choices=[('NONE', 'none'), ('NYAA', 'nyaa')], default='NONE', max_length=16)),
                ('source', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Episode',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('num', models.PositiveIntegerField()),
                ('title', models.CharField(blank=True, max_length=128, null=True)),
                ('pubdate', models.DateField(blank=True, null=True)),
                ('show', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='shows.show')),
            ],
        ),
    ]
