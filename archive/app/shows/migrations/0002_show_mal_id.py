# Generated by Django 3.2.13 on 2022-06-04 23:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shows', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='show',
            name='mal_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
