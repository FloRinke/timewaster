from django.db import models
from django.urls import reverse
from .helper import nyaa


class SOURCES(models.TextChoices):
    NONE = 'NONE', 'none'
    NYAA = 'NYAA', 'nyaa'


class Show(models.Model):
    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    name_en = models.CharField(max_length=255, null=True, blank=True)
    name_jp = models.CharField(max_length=255, null=True, blank=True)
    name_jp_romaji = models.CharField(max_length=255, null=True, blank=True)
    comment = models.CharField(max_length=255, null=True, blank=True)
    mal_id = models.PositiveIntegerField(null=True, blank=True, unique=True)
    release_date = models.DateField(null=True, blank=True)
    source_type = models.CharField(max_length=16, choices=SOURCES.choices, default=SOURCES.NONE)
    source = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        pass

    def __str__(self):
        return str(self.name_en)

    def get_absolute_url(self):
        return reverse("show:show_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("show:show_update", args=(self.pk,))

    def get_sources(self) -> list:
        if self.source_type == SOURCES.NYAA:
            if self.source is not None and self.source != "":
                print(f"Fetching source {self.source_type}:{self.source}")
        data = nyaa.get_feed(str(self.source))
        return data


class Episode(models.Model):
    # Relationships
    show = models.ForeignKey(Show, on_delete=models.PROTECT)

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    num = models.PositiveIntegerField()
    title = models.CharField(max_length=128, blank=True, null=True)
    pubdate = models.DateField(blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        if self.title is not None:
            return f"{self.num:02} - {self.title}"
        else:
            return f"{self.num:02}"

    def get_absolute_url(self):
        return reverse("show:episode_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("show:episode_update", args=(self.pk,))
