from rest_framework import serializers

from . import models


class ShowSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Show
        fields = [
            "name_en",
            "name_jp_romaji",
            "name_jp",
            "comment",
            "release_date",
            "source_type",
            "source",
        ]
        readonly_fields = [
            "created",
            "last_updated",
        ]


class EpisodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Episode
        fields = [
            "num",
            "title",
            "pubdate",
        ]
        readonly_fields = [
            "created",
            "last_updated",
        ]
