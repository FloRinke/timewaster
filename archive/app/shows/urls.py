from django.urls import path, include
from rest_framework import routers

from . import api
from . import views

app_name = "shows"

router = routers.DefaultRouter()
router.register("show", api.ShowViewSet)
router.register("episode", api.EpisodeViewSet)

urlpatterns = (
    path("api/v1/", include(router.urls)),

    path("show/", views.ShowListView.as_view(), name="show_list"),
    path("show/create", views.ShowCreateView.as_view(), name="show_create"),
    path("show/import", views.ShowImportMalView.as_view(), name="show_import_mal"),
    path("show/<int:pk>", views.ShowDetailView.as_view(), name="show_detail"),
    path("show/<int:pk>/update", views.ShowUpdateView.as_view(), name="show_update"),
    path("show/<int:pk>/watch", views.ShowWatchView.as_view(), name="show_watch"),

    path("episode/", views.EpisodeListView.as_view(), name="episode_list"),
    path("episode/create/", views.EpisodeCreateView.as_view(), name="episode_create"),
    path("episode/<int:pk>", views.EpisodeDetailView.as_view(), name="episode_detail"),
    path("episode/<int:pk>/update", views.EpisodeUpdateView.as_view(), name="episode_update"),
)
