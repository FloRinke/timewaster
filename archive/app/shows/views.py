from django.views import generic

from . import forms
from . import models
from .helper import nyaa


class ShowListView(generic.ListView):
    model = models.Show


class ShowCreateView(generic.CreateView):
    model = models.Show
    form_class = forms.ShowForm


class ShowImportMalView(generic.CreateView):
    model = models.Show
    form_class = forms.ShowImportForm


class ShowDetailView(generic.DetailView):
    model = models.Show


class ShowWatchView(generic.DetailView):
    model = models.Show

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.source_type == models.SOURCES.NYAA:
            if self.object.source is not None and self.object.source != "":
                print(f"Fetching source {self.object.source_type}:{self.object.source}")
                # context['sources'] = nyaa.get_feed(self.object.source)
                context['sources'] = self.object.get_sources()
        return context


class ShowUpdateView(generic.UpdateView):
    model = models.Show
    form_class = forms.ShowForm


class EpisodeListView(generic.ListView):
    model = models.Episode


class EpisodeCreateView(generic.CreateView):
    model = models.Episode
    form_class = forms.EpisodeForm


class EpisodeDetailView(generic.DetailView):
    model = models.Episode


class EpisodeUpdateView(generic.UpdateView):
    model = models.Episode
