import os

if os.environ.get("DJANGO_ENV") in ('prod', 'production'):
    from .production import *
elif os.environ.get("DJANGO_ENV") in ('dev', 'devel', 'development'):
    from .development import *
elif os.environ.get("DJANGO_ENV") in ('ci', 'cicd'):
    from .ci import *
else:
    raise ValueError("SET DJANGO_ENV to development, production or ci")
