from .base import *

DEBUG = False
ALLOWED_HOSTS = os.getenv('DJANGO_ALLOWED_HOSTS', 'localhost').split(',')

#CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        'LOCATION': '127.0.0.1:11211',
#    }
#}

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

STATIC_URL = '/file/static/'
STATIC_ROOT = '/srv/file/static'

MEDIA_URL = '/file/media/'
MEDIA_ROOT = '/srv/file/media/'

LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/accounts/login/'

#SESSION_COOKIE_DOMAIN = ""
#CSRF_COOKIE_DOMAIN = ""
SESSION_COOKIE_SAMESITE = 'Lax'
CSRF_COOKIE_SECURE=False
SESSION_COOKIE_SECURE=False
