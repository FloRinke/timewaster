from .base import *

DEBUG = True
ALLOWED_HOSTS = ['127.0.0.1', 'localhost', ]

INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': '',
}

STATIC_URL = '/file/static/'
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static'), ]

MEDIA_URL = '/file/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'media')

LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/accounts/login/'

# SESSION_COOKIE_DOMAIN = ""
# CSRF_COOKIE_DOMAIN = ""
SESSION_COOKIE_SAMESITE = 'Lax'
CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False

CRISPY_FAIL_SILENTLY = False
