
CREATE ROLE timewaster_owner NOLOGIN;
CREATE USER timewaster WITH PASSWORD 'admin' IN ROLE timewaster_owner;
CREATE USER timewaster_service WITH PASSWORD 'service' IN ROLE timewaster_owner;
CREATE DATABASE timewaster;
GRANT ALL ON DATABASE timewaster TO timewaster;
GRANT ALL ON DATABASE timewaster TO timewaster_service;