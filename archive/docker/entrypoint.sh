#!/bin/sh
set -e

until psql $DJANGO_DATABASE_URL -c '\l'; do
    >&2 echo "Postgres is unavailable - sleeping"
    sleep 1
done

>&2 echo "Postgres is up - continuing"

if [ "x$DJANGO_MANAGEPY_MIGRATE" = 'xon' ]; then
    >&2 echo "Try Database migration"
    python manage.py migrate --noinput
    >&2 echo "Database migration complete"
else
    >&2 echo "Not trying database migration ($DJANGO_MANAGEPY_MIGRATE)"
fi

>&2 echo "Launching Application Server"
exec "$@"
