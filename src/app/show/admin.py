from django.contrib import admin

from . import models


class MalIdInline(admin.TabularInline):
    model = models.MalId
    readonly_fields = ["twid", "uri", ]
    fields = ["mal_type", "mal_id"]
    extra = 0


class NyaaSrcInline(admin.TabularInline):
    model = models.NyaaSource
    readonly_fields = ["uri", ]
    fields = ["user", "term"]
    extra = 0


class NyaaCacheItemInline(admin.TabularInline):
    model = models.NyaaCacheItem
    readonly_fields = ["timestamp", "pub_date", ]
    fields = ["guid", "timestamp", "season", "episode", "revision", "title", "pub_date", ]
    extra = 0


@admin.register(models.NyaaCacheItem)
class NyaaCacheItemAdmin(admin.ModelAdmin):
    list_display = ["source", "timestamp", "season", "episode", "revision", "title", ]
    readonly_fields = ["timestamp", "pub_date", ]
    fields = ["guid", "timestamp", "season", "episode", "revision", "title", "pub_date", ]
    extra = 0


@admin.register(models.Show)
class ShowAdmin(admin.ModelAdmin):
    list_display = ["name", "name_native", "release_date", "broadcast_day", "broadcast_time", "episodes", ]
    readonly_fields = ["created", "last_updated", ]
    inlines = [MalIdInline, NyaaSrcInline]
    fields = [
        "created", "last_updated",
        "name", "name_native",
        "release_date", "broadcast_day", "broadcast_time",
        "episodes",
    ]


@admin.register(models.MalId)
class MalIdAdmin(admin.ModelAdmin):
    model = models.MalId
    list_display = ["show", "twid", "mal_type", "mal_id", ]
    readonly_fields = ["twid", "uri", ]
    fields = ["show", "mal_type", "mal_id", "twid", "uri", ]


@admin.register(models.NyaaSource)
class NyaaSrcAdmin(admin.ModelAdmin):
    model = models.NyaaSource
    readonly_fields = ["uri", ]
    list_display = ["show", "cached_at", "latest_release_date", "user", "term", ]
    fields = ["show", "cached_at", "latest_release_date", "user", "term"]
    inlines = [NyaaCacheItemInline]
