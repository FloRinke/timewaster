import requests
from django.conf import settings

from ..models import Show, MalId

dow = {
    'monday': 1,
    'tuesday': 2,
    'wednesday': 3,
    'thursday': 4,
    'friday': 5,
    'saturday': 6,
    'sunday': 7
}


def update_show_from_mal(show: Show, anime_id: int, fields: list[str] = None) -> int:
    headers = {'X-MAL-CLIENT-ID': settings.TOKEN_MAL}
    baseurl = "https://api.myanimelist.net/v2/anime/"
    params = "fields=alternative_titles,start_date,broadcast,num_episodes"
    result = requests.get(f"{baseurl}{anime_id}?{params}", headers=headers)
    anime = result.json()

    if fields is None or (isinstance(fields, list) and "name" in fields):
        if 'en' in anime["alternative_titles"] and anime["alternative_titles"]['en']:
            show.name = anime["alternative_titles"]['en']
            show.name_native = anime['title']
        else:
            show.name = anime['title']

    if fields is None or (isinstance(fields, list) and "release_date" in fields):
        if 'start_date' in anime:
            show.release_date = anime["start_date"]

    if fields is None or (isinstance(fields, list) and "broadcast" in fields):
        if "broadcast" in anime:
            show.broadcast_day = dow[anime["broadcast"]["day_of_the_week"]]
            show.broadcast_time = anime["broadcast"]["start_time"]

    if fields is None or (isinstance(fields, list) and "episodes" in fields):
        if "num_episodes" in anime:
            show.episodes = anime["num_episodes"]

    return anime['id']


def create_show_from_mal(anime_id):
    s, created = Show.objects.get_or_create(ext_id__mal_id=anime_id)

    if created:
        ret_id = update_show_from_mal(s, anime_id)

        s.save()

        m = MalId()
        m.show = s
        m.mal_id = ret_id
        m.save()

    return s
