import requests
from django.conf import settings

dow = {
    'monday': 1,
    'tuesday': 2,
    'wednesday': 3,
    'thursday': 4,
    'friday': 5,
    'saturday': 6,
    'sunday': 7
}


def get_show_episodes(anime_id: int) -> int | None:
    headers = {'X-MAL-CLIENT-ID': settings.TOKEN_MAL}
    baseurl = "https://api.myanimelist.net/v2/anime/"
    params = "fields=num_episodes"
    result = requests.get(f"{baseurl}{anime_id}?{params}", headers=headers)
    anime = result.json()

    if "num_episodes" in anime:
        return anime["num_episodes"]
    else:
        return None
