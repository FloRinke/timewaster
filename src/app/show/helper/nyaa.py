import datetime
import re

import feedparser

RE_EPISODECOUNT = [
    re.compile(r'S(?P<season>\d{2})E(?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?'),
    re.compile(r'(?P<season>\d+)x(?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?'),
    re.compile(r' - (?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?\b'),
    re.compile(r'\b(?P<episode>\d{2})(?:(?: \[)?v(?P<increment>\d+)]?)?\b'),
]

RE_ISBATCH = re.compile(r'Season|Batch')


def parse_episodecount(haystack: str):
    for exp in RE_EPISODECOUNT:
        m = exp.search(haystack)
        if m is not None:
            return m.groupdict()
    return dict()


def get_feed(url: str):
    f = feedparser.parse(url)
    episodes = []
    for ep in f.entries:
        is_batch = RE_ISBATCH.search(ep.title) is not None
        if not is_batch:
            episode_data = {'title': ep.title, 'size': ep.nyaa_size, 'dl': ep.link, 'guid': ep.guid, 'pub_date': datetime.datetime(*ep.published_parsed[:6], tzinfo=datetime.timezone.utc)}
            epcount = parse_episodecount(ep.title).items()
            episode_data.update({k: int(v) for k, v in epcount if v is not None})
            episodes.append(episode_data)
    return episodes

