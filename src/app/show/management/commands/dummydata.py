from django.core.management.base import BaseCommand
import random
from show import models
import logging
import datetime
from zoneinfo import ZoneInfo

logger = logging.getLogger(__name__)

SEASONDATA = {
    "2023q4": [
        {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=\"kaisen+-\"",
            "time": 42356,
            "title_en": "Jujutsu Kaisen 2nd Season",
            "malid": "51009"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=frieren",
            "time": 52300,
            "title_en": "Frieren: Beyond Journey's End",
            "title_jp": "Sousou no Frieren",
            "start": "2023-09-29",
            "malid": "52991"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=crimson",
            "time": 70100,
            "title_en": "Ragna Crimson",
            "start": "2023-10-01",
            "malid": "51297"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=migi+dali",
            "time": 12200,
            "title_en": "Migi & Dali",
            "title_jp": "Migi to Dali",
            "start": "2023-10-02",
            "malid": "50586"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=kamonohashi",
            "time": 12230,
            "title_en": "Ron Kamonohashi's Forbidden Deductions",
            "title_jp": "Kamonohashi Ron no Kindan Suiri",
            "start": "2023-10-02",
            "malid": "53879"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=tokyo+revengers+tenjiku",
            "time": 30000,
            "title": "Tokyo Revengers: Tenjiku-hen",
            "start": "2023-10-04",
            "malid": "54918"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=jitsuryokusha+s2",
            "time": 32230,
            "title_en": "The Eminence in Shadow Season 2",
            "title_jp": "Kage no Jitsuryokusha ni Naritakute! 2nd Season",
            "start": "2023-10-04",
            "malid": "54595"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=Judas&q=boushoku+berserk",
            "time": 40130,
            "title_en": "Berserk of Gluttony",
            "title_jp": "Boushoku no Berserk",
            "start": "2023-10-05",
            "malid": "53439"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=ninja",
            "time": 50128,
            "title": "Under Ninja",
            "start": "2023-10-06",
            "malid": "49766"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=tate+yuusha+s3",
            "time": 52100,
            "title_en": "The Rising of the Shield Hero Season 3",
            "title_jp": "Tate no Yuusha no Nariagari Season 3",
            "start": "2023-10-06",
            "malid": "40357"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=Ember_Encodes&q=unluck",
            "time": 60123,
            "title": "Undead Unluck",
            "start": "2023-10-07",
            "malid": "52741"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=Hametsu+Oukoku",
            "time": 60153,
            "title_en": "The Kingdoms of Ruin",
            "title_jp": "Hametsu no Oukoku",
            "start": "2023-10-07",
            "malid": "54362"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=paladin+s2",
            "time": 62200,
            "title_en": "The Faraway Paladin: The Lord of Rust Mountains",
            "title_jp": "Saihate no Paladin: Tetsusabi no Yama no Ou",
            "start": "2023-10-07",
            "malid": "50664"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=spy+family",
            "time": 62300,
            "title": "Spy x Family Season 2",
            "start": "2023-10-07",
            "malid": "53887"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=kikansha",
            "time": 70000,
            "title_en": "A Returner's Magic Should Be Special",
            "title_jp": "Kikansha no Mahou wa Tokubetsu desu",
            "start": "2023-10-08",
            "malid": "54852"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=stone+s3",
            "time": 42230,
            "title": "Dr. Stone: New World Part 2",
            "start": "2023-10-12",
            "malid": "55644"
        }, {
            "rss": "https://nyaa.si/?page=rss&c=0_0&f=0&u=AkihitoSubsWeeklies&q=pluto",
            "time": 0,
            "title": "Pluto",
            "start": "2023-10-26",
            "malid": "35737"
        }
    ]
}


class Command(BaseCommand):
    help = 'Import show data from json file'

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help="count of dummy entries")
        parser.add_argument('--extid-mal', action='store_true', help="Create external ID reference to MAL")
        parser.add_argument('--extsrc-nyaa', action='store_true', help="Create external source reference to NYAA")
        parser.add_argument('--season', help="Create dummy entries for a season (2023q4)")

    def handle(self, *args, **kwargs):
        count: int = kwargs['count']
        extid_mal: bool = kwargs['extid_mal']
        extsrc_nyaa: bool = kwargs['extsrc_nyaa']
        season: str = kwargs['season']

        if season is None:
            for i in range(count):
                name = f"Testshow {i}"
                logger.info("Creating '%s'", name)
                show = models.Show()
                show.name = name
                show.save()

                if extid_mal:
                    malid = models.MalId()
                    malid.show = show
                    malid.mal_id = random.uniform(100000, 999999)
                    malid.save()
                if extsrc_nyaa:
                    nyaasrc = models.NyaaSource()
                    nyaasrc.show = show
                    nyaasrc.term = show.name
                    nyaasrc.save()
        else:
            if season not in SEASONDATA:
                raise ValueError(f"Season '{season}' not available, use one of {','.join(SEASONDATA.keys())}")
            for show in SEASONDATA[season]:
                s = models.Show()
                if 'title_jp' in show:
                    s.name_native = show['title_jp']
                if 'title_en' in show:
                    s.name = show['title_en']
                else:
                    s.name = show['title']
                if 'start' in show:
                    s.release_date = show['start']
                if 'time' in show:
                    # "time": 12230,
                    s.broadcast_day = show['time'] / 10000
                    s.broadcast_time = datetime.time(hour=show['time'] // 100 % 100,
                                                     minute=show['time'] % 100,
                                                     tzinfo=ZoneInfo("Asia/Tokyo"))
                s.save()
                if 'malid' in show:
                    m, created = models.MalId.objects.get_or_create(mal_id=show['malid'])
                    m.show = s
                    m.save()
                if 'rss' in show:
                    if show['rss'].startswith('https://nyaa.si/?'):
                        params = {x: y for x, y in (map(lambda x: x.split('='), show['rss'][17:].split('&')))}
                        kwargs = {'term': params['q']}
                        if 'u' in params:
                            # n.user = params['u']
                            kwargs['user'] = params['u']
                        n, created = models.NyaaSource.objects.get_or_create(**kwargs)
                        n.term = params['q']
                        n.show = s
                        n.save()
