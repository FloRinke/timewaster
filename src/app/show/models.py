import urllib.parse
from datetime import timedelta

import django.core.exceptions
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.http import urlencode

from .helper import mal_nomodel as mal_helper_nomodel
from .helper import nyaa as nyaa_helper


class Show(models.Model):
    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    name = models.CharField(max_length=255)
    name_native = models.CharField(max_length=255, null=True, blank=True)
    release_date = models.DateField(null=True, blank=True)
    broadcast_day = models.PositiveIntegerField(blank=True, null=True)
    broadcast_time = models.TimeField(null=True, blank=True)
    episodes = models.PositiveIntegerField(blank=True, null=True)

    # ext_id: reverse foreign key -> ExternalId
    # ext_source: reverse foreign key -> ExternalSource

    class Meta:
        pass

    def __str__(self):
        return str(self.name)

    def get_ext_uris(self):
        return [(ref.twid, ref.uri) for ref in self.ext_ids.all()]

    def get_source_uris(self):
        return [(ref.twid, ref.uri) for ref in self.ext_srcs.all()]

    def update_showdata(self):
        refs = self.ext_ids.all()
        for ref in refs:
            ref.update_showdata()

    def get_cachedata(self, season=None, episode=None, revision=None) -> list[django.db.models.QuerySet]:
        sources = self.ext_srcs.all()
        cache = []
        for src in sources:
            for cache_item in src.get_cachedata(season, episode, revision):
                cache.append(cache_item)
        return cache

    @staticmethod
    def _update_cache(sources: models.QuerySet):
        for src in sources:
            src.fetch_source_data()

    def update_cache_force(self):
        sources = self.ext_srcs.all()
        return self._update_cache(sources)

    def update_cache_promising(self):
        cache_cutoff = timezone.now() - timedelta(minutes=10)
        release_cutoff = timezone.now() - timedelta(days=6)
        sources = self.ext_srcs.filter((Q(cached_at__isnull=True) | Q(cached_at__lt=cache_cutoff)) &
                                       (Q(latest_release_date__isnull=True) | Q(latest_release_date__lt=release_cutoff))
                                       )
        return self._update_cache(sources)


class ExternalId(models.Model):
    PROVIDER_SLUG = "DUMMY"
    DB_RELATEDNAME = f'ext_ids_{PROVIDER_SLUG.lower()}' if PROVIDER_SLUG != "DUMMY" else 'ext_ids'
    DB_RELATEDQUERYNAME = f'ext_id_{PROVIDER_SLUG.lower()}' if PROVIDER_SLUG != "DUMMY" else 'ext_id'

    show = models.ForeignKey(Show, null=True, blank=True, on_delete=models.SET_NULL,
                             related_name=DB_RELATEDNAME, related_query_name=DB_RELATEDQUERYNAME)

    @property
    def extid(self) -> str:
        return "NA"

    @property
    def extid_opt(self) -> str | None:
        return None

    @property
    def twid(self) -> str:
        if self.extid_opt:
            return f'{self.PROVIDER_SLUG}[{self.extid_opt}]:{self.extid}'
        else:
            return f'{self.PROVIDER_SLUG}:{self.extid}'

    @property
    def uri(self):
        return "http://domain.invalid/NA"

    def __str__(self):
        return self.twid

    def update_showdata(self):
        raise NotImplemented

    class Meta:
        abstract = True


class MalId(ExternalId):
    class MalType(models.TextChoices):
        Anime = 'A', 'Anime'
        Manga = 'M', 'Manga'

    PROVIDER_SLUG = "MAL"
    PROVIDER_BASEURL = "https://myanimelist.net"
    PROVIDER_TYPEPATH = {
        MalType.Anime: '/anime',
        MalType.Manga: '/manga',
    }

    mal_type = models.CharField(max_length=1, choices=MalType.choices, default=MalType.Anime)
    mal_id = models.PositiveIntegerField(unique=True)

    @property
    def extid(self):
        return self.mal_id

    @property
    def extid_opt(self):
        return self.mal_type

    @property
    def uri(self):
        return f'{self.PROVIDER_BASEURL}{self.PROVIDER_TYPEPATH[self.MalType(self.mal_type)]}/{self.mal_id}'

    def update_showdata(self):
        s = self.show
        s.episodes = mal_helper_nomodel.get_show_episodes(self.mal_id)
        s.save()


class ExternalSource(models.Model):
    SOURCE_SLUG = "DUMMY"
    DB_RELATEDNAME = f'ext_srcs_{SOURCE_SLUG.lower()}' if SOURCE_SLUG != "DUMMY" else 'ext_srcs'
    DB_RELATEDQUERYNAME = f'ext_src_{SOURCE_SLUG.lower()}' if SOURCE_SLUG != "DUMMY" else 'ext_src'

    show = models.ForeignKey(Show, null=True, blank=True, on_delete=models.SET_NULL,
                             related_name=DB_RELATEDNAME, related_query_name=DB_RELATEDQUERYNAME)
    cached_at = models.DateTimeField(blank=True, null=True)
    latest_release_date = models.DateTimeField(blank=True, null=True)

    @property
    def twid(self) -> str:
        return f'{self.SOURCE_SLUG}:{self.id}'

    @property
    def uri(self):
        return "http://domain.invalid/NA"

    def fetch_source_data(self, update_cache: bool = True) -> list:
        raise NotImplemented

    def get_cachedata(self, season=None, episode=None, revision=None) -> django.db.models.QuerySet:
        raise NotImplemented

    def __str__(self):
        return f'{self.SOURCE_SLUG}'

    class Meta:
        abstract = True


class NyaaSource(ExternalSource):
    SOURCE_SLUG = 'NYAA'
    SOURCE_BASEURL = 'https://nyaa.si/'
    # SOURCE_DEFAULTPARAMS = {'page': 'rss', 'c': '0_0', 'f': 0}
    SOURCE_DEFAULTPARAMS = {}
    term = models.CharField(max_length=255)
    user = models.CharField(max_length=255, null=True, blank=True)

    @property
    def uri(self):
        return self.get_uri()

    def get_uri(self, params: dict = {}):
        return NyaaSource.build_uri(str(self.term), str(self.user), **params)

    @staticmethod
    def build_uri(term: str, user: str = None, **kwargs):
        query = NyaaSource.SOURCE_DEFAULTPARAMS.copy()
        query['q'] = str(term)
        if user is not None:
            query['u'] = str(user)
        for kw in kwargs:
            query[kw] = kwargs[kw]
        urlparams = urlencode(query)
        return f'{NyaaSource.SOURCE_BASEURL}?{urlparams}'

    def __str__(self):
        if self.user:
            return f'{self.SOURCE_SLUG}[{self.user}]:{urllib.parse.quote_plus(str(self.term))}'
        else:
            return f'{self.SOURCE_SLUG}:{urllib.parse.quote_plus(str(self.term))}'

    def get_cachedata(self, season=None, episode=None, revision=None) -> django.db.models.QuerySet:
        cache = self.cache_items.all()
        if season is not None:
            cache = cache.exclude(season__lt=season)
            if episode is not None and episode > 0:
                if revision is None or revision == 0:
                    cache = cache.exclude(season=season, episode__lte=episode)
                else:
                    cache = cache.exclude(season=season, episode__lt=episode)
                    cache = cache.exclude(season=season, episode=episode, revision__lte=revision)
        return cache.order_by('-season', '-episode', '-revision')

    def fetch_source_data(self, update_cache: bool = True) -> list:
        uri = self.get_uri({'page': 'rss'})
        data = nyaa_helper.get_feed(uri)
        if update_cache:
            self._update_cache(data)
        return data

    def _update_cache(self, data: list[dict]) -> None:
        # data: list(dict(title, size, dl, season, episode, increment, guid, pub_date))
        if self.cached_at is not None and self.cached_at > (timezone.now() - timedelta(minutes=5)):
            raise django.core.exceptions.BadRequest("Rate limit exceeded")
        self.cache_items.update(outdated=True)  # First mark all db objects outdated
        for item in data:
            try:
                dflts = {
                    # "show": self.show,
                    "title": item['title'],
                    "url": item['dl'],
                    "episode": item['episode'],
                    "pub_date": item['pub_date'],
                    "season": item['season'] if 'season' in item else 0,
                }
            except KeyError:
                continue

            obj, created = self.cache_items.get_or_create(guid=item['guid'], defaults=dflts)
            if created:
                if 'increment' in item:
                    obj.increment = item['increment']
            obj.outdated = False  # mark refreshed objects as non-outdated, so those missing in rss remain marked
            obj.save()
            if self.latest_release_date is None or item['pub_date'] > self.latest_release_date:
                self.latest_release_date = item['pub_date']
        self.cached_at = timezone.now()
        self.save()


class CacheItem(models.Model):
    source = models.ForeignKey(ExternalSource, on_delete=models.CASCADE,
                               related_name="cache_items", related_query_name="cache_item")
    timestamp = models.DateTimeField(auto_now=True, editable=False)
    outdated = models.BooleanField(default=False)

    # def __str__(self):
    #     return f'{self.SOURCE_SLUG}'

    class Meta:
        abstract = True


class NyaaCacheItem(CacheItem):
    source = models.ForeignKey(NyaaSource, on_delete=models.CASCADE,
                               related_name="cache_items", related_query_name="cache_item")
    guid = models.CharField(max_length=255, unique=True)
    pub_date = models.DateTimeField()
    season = models.PositiveIntegerField(blank=True, null=True)
    episode = models.PositiveIntegerField()
    revision = models.PositiveIntegerField(blank=True, null=True)
    title = models.CharField(max_length=255)
    url = models.URLField(unique=True)

    def __str__(self):
        return f'[{self.timestamp}] {self.source.show} {self.episode_counter}'

    @property
    def episode_counter(self):
        if self.season <= 1:
            out = f'{self.episode}'
        else:
            out = f'{self.season}x{self.episode:02}'

        if self.revision is not None:
            out += f'v{self.revision}'

        return out
