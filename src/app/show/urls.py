from django.urls import path, include

from . import views

app_name = "show"

urlpatterns = (
    path("", views.ShowListView.as_view(), name="list"),
    path("<int:pk>", views.ShowDetailView.as_view(), name="detail"),
    path("<int:pk>/watch", views.ShowWatchView.as_view(), name="watch"),
    path("<int:pk>/refresh", views.refresh_show_cache, name="refresh"),
    path("<int:pk>/update", views.update_show, name="update"),
    path("<int:sid>/add/src/nyaa", views.AddNyaaSourceView.as_view(), name="add_src_nyaa"),
)
