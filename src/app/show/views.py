from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from . import models
from .helper import nyaa as nyaa_helper


class ShowListView(LoginRequiredMixin, generic.ListView):
    model = models.Show


class ShowDetailView(LoginRequiredMixin, generic.DetailView):
    model = models.Show

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for s in self.object.ext_srcs.all():
            context['source_cache'] = s.cache_items.filter(outdated=False)
        return context


class ShowWatchView(ShowDetailView):
    # inherits LoginRequiredMixin
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for s in self.object.ext_srcs.all():
            context['sources'] = s.fetch_source_data()
        return context


@login_required
def refresh_show_cache(request, pk: int):
    obj = models.Show.objects.get(id=pk)
    obj.update_cache_force()
    if 'redirect' in request.POST:
        return HttpResponseRedirect(request.POST['redirect'])
    elif 'redirect' in request.GET:
        return HttpResponseRedirect(request.GET['redirect'])
    else:
        return HttpResponseRedirect(reverse('show:detail', args=(obj.id,)))


@login_required
def update_show(request, pk: int):
    obj = models.Show.objects.get(id=pk)
    obj.update_showdata()
    if 'redirect' in request.POST:
        return HttpResponseRedirect(request.POST['redirect'])
    elif 'redirect' in request.GET:
        return HttpResponseRedirect(request.GET['redirect'])
    else:
        return HttpResponseRedirect(reverse('show:detail', args=(obj.id,)))


class AddNyaaSourceView(LoginRequiredMixin, generic.CreateView):
    model = models.NyaaSource
    fields = ('user', 'term')

    def form_valid(self, form):
        if 'create' in self.request.POST:
            self.object = form.save(commit=False)
            self.object.show = models.Show.objects.get(id=self.kwargs['sid'])
            self.object.save()
            return super().form_valid(form)
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse('show:detail', args=(self.object.show.id,))

    def get_initial(self):
        initials = {}
        s = models.Show.objects.get(id=self.kwargs['sid'])
        if 'user' in self.request.GET:
            initials['user'] = self.request.GET['user']

        if 'term' in self.request.GET:
            initials['term'] = self.request.GET['term']
        else:
            initials['term'] = s.name

        return initials

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show'] = models.Show.objects.get(id=self.kwargs['sid'])
        context['today'] = timezone.now()
        params = {
            'page': 'rss',
            'f': '0',  # no filter
            'c': '1_2',  # anime, english translated
        }
        if 'form' in kwargs and 'user' in kwargs['form'].cleaned_data:
            params['user'] = kwargs['form'].cleaned_data['user']
            uri = models.NyaaSource.build_uri(kwargs['form'].cleaned_data['term'], **params)
            context['source_uri'] = uri
            rss = nyaa_helper.get_feed(uri)
            context['source_data'] = [{'date': x['pub_date'], 'title': x['title'], 'size': x['size']} for x in rss]
        return context
