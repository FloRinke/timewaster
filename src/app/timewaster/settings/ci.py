from .base import *

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DATABASES = {
    'default': dj_database_url.config(
        default='sqlite:///' + os.path.abspath(os.path.join(BASE_DIR, '..', 'db.sqlite3')),
        conn_max_age=600,
        conn_health_checks=True,
    )
}