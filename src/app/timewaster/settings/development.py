from .base import *

DEBUG = True

# DJango Debug toolbar
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]
INSTALLED_APPS += ['debug_toolbar', ]
INTERNAL_IPS = ["127.0.0.1", ]
DEBUG_TOOLBAR_CONFIG = {'JQUERY_URL': os.path.join(STATIC_ROOT, 'js', 'jquery-3.7.1.slim.min.js'), }

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ROOT_URLCONF = 'timewaster.urls_dev'

STATICFILES_DIRS = [os.path.abspath(os.path.join(BASE_DIR, '..', '..', '_staticfiles'))]
MEDIA_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', '..', '.media'))
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', '..', '.static'))

del SESSION_COOKIE_DOMAIN
del CSRF_COOKIE_DOMAIN
SESSION_COOKIE_SAMESITE = 'Lax'
CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False
