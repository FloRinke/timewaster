"""TimeWaster URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

urlpatterns = [
    path('',                    TemplateView.as_view(template_name='index.html'), name='index'),
    path('show/',               include('show.urls',                    namespace='show')),
    path('watchlist/',          include('watchlist.urls',               namespace='watchlist')),
    path("__debug__/",          include("debug_toolbar.urls")),
    path("accounts/",           include("django.contrib.auth.urls")),
    path('admin/',              admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
