from django.contrib import admin

from . import models


class WatchListEntryInline(admin.TabularInline):
    model = models.WatchListEntry
    readonly_fields = ["created", "last_updated", ]
    fields = ["show", "status", "season", "episode", "revision", ]
    extra = 3


class WatchListPermissionInline(admin.TabularInline):
    model = models.WatchListPermission
    readonly_fields = ["created", "last_updated", ]
    fields = ["user", "access_level", ]
    extra = 1


@admin.register(models.Watchlist)
class WatchlistAdmin(admin.ModelAdmin):
    list_display = ["name", "owner", ]
    readonly_fields = ["created", "last_updated", ]
    fields = ["name", "owner", ]
    inlines = [WatchListEntryInline, WatchListPermissionInline, ]
    list_filter = ["owner", ]


@admin.register(models.WatchListEntry)
class WatchListEntryAdmin(admin.ModelAdmin):
    model = models.WatchListEntry
    list_display = ["watchlist", "show", "status", "episode_counter", ]
    readonly_fields = ["created", "last_updated", ]
    fields = ["watchlist", "show", "status", "season", "episode", "revision", ]
    list_filter = ["status", "watchlist", ]


@admin.register(models.WatchListPermission)
class WatchListPermissionAdmin(admin.ModelAdmin):
    model = models.WatchListPermission
    list_display = ["watchlist", "access_level", "user", ]
    readonly_fields = ["created", "last_updated", ]
    fields = ["watchlist", "access_level", "user", ]
    list_filter = ["watchlist", "access_level", "user", ]
