import re

from django import forms


class TwcliImportForm(forms.Form):
    sources = forms.FileField(required=False)
    seen = forms.FileField(required=False)


class MalinderImportForm(forms.Form):
    choice = forms.CharField(widget=forms.Textarea)
    watchlist = forms.CharField()

    def clean_choice(self):
        choice = self.cleaned_data['choice']
        result = re.findall(r"https://myanimelist.net/anime/(\d{5,})", choice, re.MULTILINE)
        return result
