import datetime

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone

import show.models as show_models


class Watchlist(models.Model):
    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    name = models.CharField(max_length=255)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)

    def __repr__(self) -> str:
        return f"{self.__class__}({self.name})"

    def __str__(self) -> str:
        return str(self.name)

    def get_new_watchlinks(self):
        data = []
        for entry in self.get_active_entries():
            for ep in entry.get_new_episodes():
                data.append({'show': ep.source.show, 'episode': ep.episode_counter, 'url': ep.url})
        return data

    def get_active_entries(self):
        filter_active = Q(status=WatchListEntry.WatchStatus.Import) \
                        | Q(status=WatchListEntry.WatchStatus.Plan) \
                        | Q(status=WatchListEntry.WatchStatus.Watch)
        return self.entries.filter(filter_active)

    def get_backlog_entries(self):
        filter_active = Q(status=WatchListEntry.WatchStatus.Backlog)
        return self.entries.filter(filter_active)

    class Meta:
        ordering = ('name',)


class WatchListPermission(models.Model):
    class AccessLevel(models.TextChoices):
        L10_View = 'L10', 'L10_View'
        L20_Refresh = 'L20', 'L20_Refresh'
        L30_Edit = 'L30', 'L30_Mark'
        L40_Edit = 'L40', 'L40_Edit'
        L50_Manage = 'L50', 'L50_Manage'
        L60_Successor = 'L60', 'L60_Successor'

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    watchlist = models.ForeignKey(Watchlist,
                                  on_delete=models.CASCADE,
                                  related_name='permission',
                                  related_query_name='permissions')

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    access_level = models.CharField(max_length=3, choices=AccessLevel.choices)

    def __repr__(self) -> str:
        return f"{self.__class__}({self.watchlist}, {self.access_level}, {self.user})"

    def __str__(self) -> str:
        return f"{self.watchlist}[{self.access_level}]: {self.user}"

    class Meta:
        ordering = ('access_level', 'user',)


class WatchListEntry(models.Model):
    class WatchStatus(models.TextChoices):
        Undefined = 'U', 'Undefined'
        Backlog = 'B', 'Backlog'
        Plan = 'P', 'PlanToWatch'
        Watch = 'W', 'Watching'
        Complete = 'C', 'Complete'
        Drop = 'D', 'Dropped'
        Import = 'I', 'Imported'

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    watchlist = models.ForeignKey(Watchlist,
                                  on_delete=models.CASCADE,
                                  related_name='entries',
                                  related_query_name='entry')
    status = models.CharField(max_length=1, choices=WatchStatus.choices)

    show = models.ForeignKey(show_models.Show, on_delete=models.CASCADE)
    season = models.PositiveIntegerField(default=0)
    episode = models.PositiveIntegerField(default=0)
    revision = models.PositiveIntegerField(default=0)

    def __str__(self) -> str:
        return f'{self.watchlist}[{self.status},S{self.episode_counter}]({self.show})'

    class Meta:
        ordering = ('-status', 'show__name',)

    @property
    def episode_counter(self):
        if self.season == 0:
            out = f'{self.episode}'
        else:
            out = f'{self.season}x{self.episode:02}'

        if self.revision > 1:
            out += f'v{self.revision}'

        return out

    @property
    def cache_refresh_date(self) -> datetime.datetime:
        try:
            qs = self.show.ext_srcs.get().cache_items.all().order_by('-timestamp')[0]
            return qs.timestamp
        except IndexError:
            return None
        except show_models.NyaaSource.MultipleObjectsReturned:
            # if there are multiple sources, use newest of them
            ts = []
            for src in self.show.ext_srcs.all():
                try:
                    ts.append(src.cache_items.all().order_by('-timestamp')[0].timestamp)
                except IndexError:
                    pass  # no items in cache
            if len(ts) == 0:
                # no sources had any cached data
                return None
            return max(ts)

    @property
    def cache_release_date(self) -> datetime.datetime:
        try:
            qs = self.show.ext_srcs.get().cache_items.all().order_by('-season', '-episode', '-revision')[0]
            return qs.pub_date
        except IndexError:
            return None
        except show_models.NyaaSource.MultipleObjectsReturned:
            pd = []
            for src in self.show.ext_srcs.all():
                try:
                    pd.append(src.cache_items.all().order_by('-season', '-episode', '-revision')[0].pub_date)
                except IndexError:
                    pass
            if len(pd) == 0:
                # no sources had any cached data
                return None
            return max(pd)

    @property
    def cache_episode_counter(self):
        try:
            qs = self.show.ext_srcs.get().cache_items.all().order_by('-season', '-episode', '-revision')[0]
            return qs.episode_counter
        except IndexError:
            return ""
        except show_models.NyaaSource.MultipleObjectsReturned:
            ec = []
            for src in self.show.ext_srcs.all():
                try:
                    ec.append(src.cache_items.all().order_by('-season', '-episode', '-revision')[0].episode_counter)
                except IndexError:
                    pass
            return ", ".join(ec)

    def get_new_watchlinks(self):
        data = self.show.get_cachedata(self.season, self.episode, self.revision)
        return [ep.url for ep in data]

    def get_watchlinks(self):
        data = self.show.get_cachedata()
        return [ep.url for ep in data]

    def get_new_episodes(self):
        if self.status == self.WatchStatus.Import:
            return self.show.get_cachedata()
        else:
            return self.show.get_cachedata(self.season, self.episode, self.revision)

    def get_episodes(self):
        return self.show.get_cachedata()

    @property
    def release_age(self) -> int:
        try:
            return (timezone.now() - self.cache_release_date).total_seconds()
        except ValueError:
            return 0
        except TypeError:  # most likely cache_release_date returned 'None'
            return 0

    @property
    def is_release_overdue(self) -> bool:
        return self.release_age > 604800  # 7*24*60*60 = 1w

    @property
    def release_expected_date(self) -> datetime:
        return self.cache_release_date + datetime.timedelta(days=7)

    @property
    def release_progress(self) -> int:
        if self.release_age <= 604800:  # 7*24*60*60 = 1w
            return self.release_age
        else:
            return 604800

    @property
    def release_overdue(self) -> int:
        if self.release_age <= 604800:  # 7*24*60*60 = 1w
            return 0
        else:
            return self.release_age - 604800

    @property
    def release_progress_percent(self) -> int:
        if not self.is_release_overdue:
            return round(self.release_progress / 604800 * 100)
        else:
            return round(self.release_progress / self.release_age * 100)

    @property
    def release_overdue_percent(self) -> int:
        if self.is_release_overdue:
            return round(self.release_overdue / self.release_age * 100)
        else:
            return 0

    def ack(self, season: int | None, episode: int, revision: int = None, force: bool = False):
        if self.status == self.WatchStatus.Import:
            force = True
            self.status = self.WatchStatus.Watch

        if season is None:
            season = 0
        if revision is None:
            revision = 0

        if not force:
            if season < self.season:
                return False
            elif season == self.season and episode < self.episode:
                return False
            elif season == self.season and episode == self.episode and revision <= self.revision:
                return False

        if self.status == self.WatchStatus.Watch and episode == self.show.episodes:
            self.status = self.WatchStatus.Complete

        self.season = season
        self.episode = episode
        self.revision = revision
        self.save()
