from django.urls import path

from . import views

app_name = "watchlist"

urlpatterns = (
    path("", views.WatchlistListView.as_view(), name="list"),
    path("<int:pk>", views.WatchlistDetailView.as_view(), name="detail"),
    path("<int:pk>/curl", views.WatchlistCurlView.as_view(), name="curl"),
    path("<int:pk>/refresh/all", views.refresh_all, name="refresh_all"),
    path("<int:pk>/refresh/promising", views.refresh_promising, name="refresh_promising"),
    path("<int:pk>/<int:item>/ack", views.ack, name="ack"),
    path("entries/<int:pk>", views.WatchlistEntryDetailView.as_view(), name="entry_detail"),
    path("entries/<int:pk>/edit", views.WatchlistEntryUpdateView.as_view(), name="entry_update"),
    # path("entries/<int:pk>/delete", views.WatchlistEntryDeleteView.as_view(), name="entry_delete"),
    path("import_twcli", views.WatchlistTwcliImportView.as_view(), name="import_twcli"),
    path("import_malinder", views.MalinderImportView.as_view(), name="import_malinder"),
)
