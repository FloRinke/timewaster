import json
from urllib.parse import urlparse

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

import show.helper.mal as mal_helper
import show.models as show_models
from . import models, forms


def _get_queryset_watchlist_visible(request):
    filter_exp = Q(owner=None) | Q(owner=request.user) | Q(permissions__user=request.user)
    return models.Watchlist.objects.filter(filter_exp).distinct()


class WatchlistListView(LoginRequiredMixin, generic.ListView):
    model = models.Watchlist

    def get_queryset(self):
        return _get_queryset_watchlist_visible(self.request)


class WatchlistDetailView(LoginRequiredMixin, generic.DetailView):
    model = models.Watchlist

    def get_queryset(self):
        return _get_queryset_watchlist_visible(self.request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = timezone.now().date()
        return context


class WatchlistEntryDetailView(LoginRequiredMixin, generic.DetailView):
    model = models.WatchListEntry

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = timezone.now().date()
        return context


class WatchlistEntryUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = models.WatchListEntry
    fields = ('watchlist', 'status')  # , 'season', 'episode', 'revision')

    def get_success_url(self):
        return reverse("watchlist:entry_detail", kwargs={'pk': self.object.id})


# class WatchlistEntryDeleteView(LoginRequiredMixin, generic.DeleteView):
#     model = models.WatchListEntry


@method_decorator(csrf_exempt, name='dispatch')
class WatchlistCurlView(LoginRequiredMixin, generic.DetailView):
    model = models.Watchlist
    template_name = "watchlist/watchlist_curl.txt"
    fields = []
    content_type = "text"

    def post(self, request, *args, **kwargs):
        body = self.request.body.decode('utf-8').split('\n')
        pk = kwargs['pk']
        object = self.model.objects.get(pk=pk)
        # print(f"received {body}")
        for link in body:
            if not link:
                continue
            # print(f"  checking '{link}'")
            wle = object.entries.get(show__ext_src__cache_item__url=link)
            # print(f"    matching entry {wle}")
            ci = show_models.NyaaCacheItem.objects.get(url=link)
            # print(f"    matching ci {ci}")
            # print (f"  acking S{ci.season:02}:E{ci.episode:02}v{ci.revision}")
            wle.ack(ci.season, ci.episode, ci.revision)

        return self.get(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.path


@login_required
def _refresh(request, pk, all: bool = False):
    if request.method == 'POST':
        filter_wli = Q(status=models.WatchListEntry.WatchStatus.Import) \
                     | Q(status=models.WatchListEntry.WatchStatus.Backlog) \
                     | Q(status=models.WatchListEntry.WatchStatus.Plan) \
                     | Q(status=models.WatchListEntry.WatchStatus.Watch)
        obj = _get_queryset_watchlist_visible(request).get(id=pk)
        for wli in obj.entries.filter(filter_wli):
            if all:
                wli.show.update_cache_force()
            else:
                wli.show.update_cache_promising()
        if 'redirect' in request.POST:
            return HttpResponseRedirect(request.POST['redirect'])
        if 'redirect' in request.GET:
            return HttpResponseRedirect(request.POST['GET'])
    else:
        return HttpResponseBadRequest("No watchlist specified")


@login_required
def refresh_all(request, pk):
    return _refresh(request, pk, all=True)


@login_required
def refresh_promising(request, pk):
    return _refresh(request, pk, all=False)


@login_required
def ack(request, pk, item):
    if request.method == 'POST':
        obj = models.WatchListEntry.objects.get(id=item)
        params = dict()
        for param in 'season', 'episode', 'revision':
            if param in request.GET and request.GET[param] != "None":
                params[param] = int(request.GET[param])
            else:
                params[param] = None
        obj.ack(**params)
    if 'redirect' in request.POST:
        return HttpResponseRedirect(request.POST['redirect'])
    if 'redirect' in request.GET:
        return HttpResponseRedirect(request.POST['GET'])


class WatchlistTwcliImportView(LoginRequiredMixin, generic.FormView):
    template_name = "watchlist/import_twcli_form.html"
    form_class = forms.TwcliImportForm
    success_url = reverse_lazy("watchlist:list")

    def form_valid(self, form):
        if 'sources' in self.request.FILES:
            sources_data = json.load(self.request.FILES['sources'])
        else:
            sources_data = []

        if 'seen' in self.request.FILES:
            seen_data = json.load(self.request.FILES['seen'])
        else:
            seen_data = []

        for tag in sources_data:
            wl, created = _get_queryset_watchlist_visible(self.request) \
                .get_or_create(name=tag, defaults={"owner": self.request.user})

            for slug in sources_data[tag]:
                params = {}
                if 'title' in sources_data[tag][slug] and sources_data[tag][slug]['title'] != "":
                    params['name'] = sources_data[tag][slug]['title']
                elif 'title_en' in sources_data[tag][slug] and sources_data[tag][slug]['title_en'] != "":
                    params['name'] = sources_data[tag][slug]['title_en']
                if 'title_jp' in sources_data[tag][slug] and sources_data[tag][slug]['title_jp'] != "":
                    params['name_native'] = sources_data[tag][slug]['title_jp']
                if len(params) == 0:
                    continue  # skip if there is no name
                s, created = show_models.Show.objects.get_or_create(**params)

                if 'malid' in sources_data[tag][slug]:
                    malid = sources_data[tag][slug]['malid']
                    mid, created = show_models.MalId.objects.get_or_create(show=s, mal_type='A', mal_id=malid)

                if 'rss' in sources_data[tag][slug]:
                    rss = sources_data[tag][slug]['rss']
                    u = dict((x, y) for x, y in list(map(lambda x: x.split('='), urlparse(rss).query.split('&'))))
                    params = {'show': s, 'term': u['q']}
                    if 'u' in u:
                        params['user'] = u['u']
                    nyaa, created = show_models.NyaaSource.objects.get_or_create(**params)

                wle, created = models.WatchListEntry.objects.get_or_create(show=s, watchlist=wl, defaults={'status': 'I'})

                if tag in seen_data and slug in seen_data[tag]:
                    season = seen_data[tag][slug]['season']
                    episode = seen_data[tag][slug]['episode']
                    wle.ack(season, episode)

        return super().form_valid(form)


class MalinderImportView(LoginRequiredMixin, generic.FormView):
    template_name = "watchlist/import_malinder_form.html"
    form_class = forms.MalinderImportForm
    success_url = reverse_lazy("watchlist:list")

    def form_valid(self, form):
        if 'choice' in self.request.POST:
            choice = form.cleaned_data['choice']
        else:
            choice = []
        shows = [mal_helper.create_show_from_mal(x) for x in choice]

        if 'watchlist' in self.request.POST:
            w, created = models.Watchlist.objects.get_or_create(name=form.cleaned_data['watchlist'])
            for elem in shows:
                params = {
                    "show": elem,
                    "watchlist": w,
                    "status": models.WatchListEntry.WatchStatus.Import
                }
                models.WatchListEntry.objects.get_or_create(**params)
        return super().form_valid(form)
