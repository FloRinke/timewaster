
CREATE ROLE timewaster_dev_owner NOLOGIN;
CREATE USER timewaster_dev WITH PASSWORD 'admin' IN ROLE timewaster_dev_owner;
CREATE USER timewaster_dev_service WITH PASSWORD 'service' IN ROLE timewaster_dev_owner;
CREATE DATABASE timewaster_dev;
GRANT ALL ON DATABASE timewaster_dev TO timewaster_dev;
GRANT ALL ON DATABASE timewaster_dev TO timewaster_dev_service;
