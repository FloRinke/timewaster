
CREATE ROLE timewaster_prod_owner NOLOGIN;
CREATE USER timewaster_prod WITH PASSWORD 'admin' IN ROLE timewaster_prod_owner;
CREATE USER timewaster_prod_service WITH PASSWORD 'service' IN ROLE timewaster_prod_owner;
CREATE DATABASE timewaster_prod;
GRANT ALL ON DATABASE timewaster_prod TO timewaster_prod;
GRANT ALL ON DATABASE timewaster_prod TO timewaster_prod_service;
