
CREATE ROLE timewaster_qa_owner NOLOGIN;
CREATE USER timewaster_qa WITH PASSWORD 'admin' IN ROLE timewaster_qa_owner;
CREATE USER timewaster_qa_service WITH PASSWORD 'service' IN ROLE timewaster_qa_owner;
CREATE DATABASE timewaster_qa;
GRANT ALL ON DATABASE timewaster_qa TO timewaster_qa;
GRANT ALL ON DATABASE timewaster_qa TO timewaster_qa_service;
