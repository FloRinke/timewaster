#!/bin/bash
ressources_path="$1"
echo "Installing ressources into '$ressources_path'"

bootstrap_version="5.3.3"
bootstrap_icons_version="1.11.3"
jquery_version="3.7.1"

jquery_url="https://code.jquery.com/jquery-${jquery_version}.slim.min.js"
popper_url="https://unpkg.com/@popperjs/core@2"
bootstrap_url="https://github.com/twbs/bootstrap/releases/download/v${bootstrap_version}/bootstrap-${bootstrap_version}-dist.zip"
bootstrap_icons_url="https://github.com/twbs/icons/releases/download/v${bootstrap_icons_version}/bootstrap-icons-${bootstrap_icons_version}.zip"

set -ex
# ensure path exists
mkdir -p ${ressources_path}

# get external dependencies
wget -qP "${ressources_path}/js/" "$jquery_url"
wget -qO "${ressources_path}/js/popper.min.js" "$popper_url"

if ! [ -d "${ressources_path}/bootstrap" ]; then
	wget -qP "${ressources_path}/" "$bootstrap_url"
	unzip -q "${ressources_path}/bootstrap-${bootstrap_version}-dist.zip" -d "${ressources_path}/"
	mv -v "${ressources_path}/bootstrap-${bootstrap_version}-dist" "${ressources_path}/bootstrap"
	rm -v "${ressources_path}/bootstrap-${bootstrap_version}-dist.zip"
fi
if ! [ -d "${ressources_path}/bootstrap/icons" ]; then
	wget -qP "${ressources_path}/" "$bootstrap_icons_url"
	unzip -q "${ressources_path}/bootstrap-icons-${bootstrap_icons_version}.zip" -d "${ressources_path}/"
	mv -v "${ressources_path}/bootstrap-icons-${bootstrap_icons_version}" "${ressources_path}/bootstrap/icons"
	rm -v "${ressources_path}/bootstrap-icons-${bootstrap_icons_version}.zip"
fi